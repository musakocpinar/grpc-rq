# How to usage

Go to project folder with 'cd' command

Create images:
- docker build -f ./client/Dockerfile client
- docker build -f ./server/Dockerfile server
- docker build -f ./worker/Dockerfile worker
    
Run containers
- docker-compose up

When you run 'docker-compose up', immediately workers start to work. 
Writes all files under the client/file/users/ directory to the users.json file.
You can see the number of lines added with the command 'wc -l users.json'.
If you increase the 'worker' in 'docker-compose.yml', your processing capacity will increase.
