from concurrent import futures
import transfer_pb2
import transfer_pb2_grpc
import grpc
import redis_q
import time

class CustomServicer(transfer_pb2_grpc.FileServiceServicer):
    def __init__(self):
        pass

    def CustomFile(self, request, context):
        """
        Add data from grpc to redis queue
        :param request: Grpc request
        :param context: Grpc context
        :return: response
        """
        response = transfer_pb2.FileTransfer()
        redis_q.add_to_queue(request.user)
        return response


server = grpc.server(futures.ThreadPoolExecutor(max_workers=1))
transfer_pb2_grpc.add_FileServiceServicer_to_server(CustomServicer(), server)

server.add_insecure_port('[::]:8080')
server.start()

time.sleep(1000)