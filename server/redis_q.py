import os
import redis
from rq import Queue


r = redis.Redis('redis', 6379)
q = Queue(connection=r, name="users")


def add_to_queue(text):
    """
    Add text to queue
    :param text: simple text
    :return: None
    """
    worker_module_path = os.getenv('WORKER_MODULE_PATH')
    q.enqueue(worker_module_path, text)
