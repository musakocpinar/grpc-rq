import grpc
import transfer_pb2_grpc
import transfer_pb2
from os import listdir
from os.path import isfile, join


channel = grpc.insecure_channel('server:8080')
stub = transfer_pb2_grpc.FileServiceStub(channel)


def read_from_file(path):
    """
    Read all line from files and iterate
    :param path: files path
    :return: line
    """
    with open(path) as f:
        for line in f.read().splitlines():
            yield line
        f.close()


folder_path = "files/users/"
onlyfiles = [f for f in listdir(folder_path) if isfile(join(folder_path, f))]

for file in onlyfiles:
    full_path = folder_path + file

    for data in read_from_file(full_path):
        user = transfer_pb2.FileTransfer(user=data)
        response = stub.CustomFile(user)