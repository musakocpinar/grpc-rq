def background_task(data):
    """
    Incoming data from redis worker
    :param data: Simple text
    :return: None
    """
    add_to_file(data)


def add_to_file(data):
    """
    Add data to users.json file
    :param data: Simple text
    :return: None
    """
    with open("users.json", 'a') as f:
        f.write(data + '\n')

