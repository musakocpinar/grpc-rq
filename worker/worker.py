import os
from rq import Queue, Worker
from redis import Redis
import job
import time


"""
RQ workers for perform tasks that are added to the queue.
"""

time.sleep(5)

conn1 = Redis('redis', 6379)
queue = ('users')

mode = os.getenv('WORKER_BURST')

worker = Worker([queue], connection=conn1, log_job_description=False)
worker.work(burst=mode)
